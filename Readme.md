# ownMapp Hubsite


[Hubzilla](https://grid.reticu.li/hubzilla) is a powerful platform for creating interconnected websites featuring a decentralized identity, communications, and permissions framework built using common webserver technology. Hubzilla supports identity-aware webpages, using a modular system of website construction using shareable elements including pages, layouts, menus, and blocks. 

This repository hosts the source code for the ownMapp project hubsite for development convenience. Ideally the import and update of the hubsite from these source files will be possible using a plugin for the purpose.